<?php 
	include_once 'Conexion.php';
	header("application/json");
	session_start();

	extract($_POST);
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";
	$subtotal = 0;
	$totalFinal = 0;
	$dniCliente = $_SESSION['DNICliente'];

	try {
		$stmt = $con->prepare("SELECT idCarrito, dniCliente, nombreMueble, cantidad, precioUnitario, total FROM carrito_detalle c 
			INNER JOIN mueble m ON m.idMueble = c.idMueble 
			WHERE dniCliente = ?");
		$stmt->bindParam(1, $dniCliente);
		

		if ($stmt->execute()) {
			
			while ($fila = $stmt->fetch()) {
				$subtotal += $fila["total"];
				$mensajeRespuesta = "Existen productos";
			}
			$totalFinal = $subtotal;
		}else{
			$codigoRespuesta=1;
			$mensajeRespuesta = "Error al cargar los totales";
		}

	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally{
		$json = array(
			'codigoRespuesta' => $codigoRespuesta,
			'mensajeRespuesta'=>$mensajeRespuesta,
			'subtotal' => $subtotal,
			'totalFinal'=>$totalFinal,
			'fila'=> $fila
		);
		echo json_encode($json);
	}

?>