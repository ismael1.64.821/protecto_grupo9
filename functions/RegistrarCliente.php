<?php 
	include_once 'Conexion.php';
	extract($_POST);
	header("application/json");
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";
	try {
		
		$stmt = $con->prepare("INSERT INTO cliente VALUES (?,?,?,?,?,?,?)");
		// Bind
	
		$stmt->bindParam(1, $txtNumeroDniCliente);
		$stmt->bindParam(2, $txtNombreCliente);
		$stmt->bindParam(3, $txtGenero);
		$stmt->bindParam(4, $txtTelefonoCliente);
		$stmt->bindParam(5, $txtCorreoCliente);
		$stmt->bindParam(6, $txtDireccionCliente);
		$stmt->bindParam(7, $txtclave);
		// Excecute
		
		if($stmt->execute()) {
		    $mensajeRespuesta = "Cliente registrado correctamente"; 
		}else{
			$codigoRespuesta = 1;
			$mensajeRespuesta = "Error al registrar cliente";
		}

	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally{
		$json = array('codigoRespuesta' => $codigoRespuesta,'mensajeRespuesta'=>$mensajeRespuesta);
		echo json_encode($json);
	}
?>