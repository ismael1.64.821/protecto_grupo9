<?php 
	include_once 'Conexion.php';
	extract($_POST);
	header("application/json");
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";
	try {
		
		//consulta de delete pero en el where idcarrito=?
		$stmt = $con->prepare("DELETE FROM carrito_detalle WHERE idCarrito = ?");
		// Bind
	
		$stmt->bindParam(1, $idCarrito);
	
		// Excecute
		
		if($stmt->execute()) {
		    $mensajeRespuesta = "Producto eliminado correctamente"; 
		}else{
			$codigoRespuesta = 1;
			$mensajeRespuesta = "No se pudo eliminar producto";
		}

	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally{
		$json = array('codigoRespuesta' => $codigoRespuesta,'mensajeRespuesta'=>$mensajeRespuesta);
		echo json_encode($json);
	}
?>