<?php 
	include_once 'Conexion.php';
	extract($_POST);
	header("application/json");
	session_start(); 
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";
	$espacioUsuario = "";
	$nombreCliente = "";
	$correoCliente = "";
	$DNICliente = "";
	$cbSesion = false;
	try {

		
		if (empty($_SESSION['nombreCliente'])) {
			$codigoRespuesta = 1;
			$mensajeRespuesta = "Sesion cerrada";
		}else{
			$nombreCliente = $_SESSION['nombreCliente'];
			$correoCliente = $_SESSION['correoCliente'];
			$DNICliente = $_SESSION['DNICliente'];
			$cbSesion = $_SESSION['sesionAbierta'];
			$codigoRespuesta = 0;
			$mensajeRespuesta = "Sesion abierta";

			$espacioUsuario = <<<STR
				<div class="nav-item dropdown">
					<a class="nav-link dropdown-toggle active" href="#" id="usuarioOptions" role="button" data-bs-toggle="dropdown" aria-expanded="false">$nombreCliente</a>
					<ul class="dropdown-menu dropdown-menu-end" aria-labelledby="usuarioOptions">
					    <li><a class="dropdown-item" href="#">Ver Perfil</a></li>
					    <li><a class="dropdown-item" href="misCompras.html">Mis Compras</a></li>
					    <li><hr class="dropdown-divider"></li>
					    <li><a class="dropdown-item" href="#" data-bs-toggle="modal" data-bs-target="#modalConfirmacionCerrarSesion">Cerrar Sesión</a></li>
					</ul>
				</div>
			STR;
		}
		

	} catch (Exception $e) {
		
	}finally{
		$json = array(
			'codigoRespuesta' => $codigoRespuesta,
			'mensajeRespuesta'=>$mensajeRespuesta,
			'espacioUsuario' => $espacioUsuario,
			'mantenerSesion' => $_SESSION['sesionAbierta'] = $cbSesion
		);
		echo json_encode($json);
	}
?>