<?php 
	include_once 'Conexion.php';
	header("application/json");
	session_start();

	extract($_POST);
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";

	try {
		$stmt = $con->prepare("SELECT idCarrito, dniCliente, nombreMueble, cantidad, precioUnitario, total FROM carrito_detalle c 
			INNER JOIN mueble m ON m.idMueble = c.idMueble 
			WHERE idCarrito = ?");
		$stmt->bindParam(1, $idCarrito);
		$stmt->execute();
		if ($fila = $stmt->fetch()) {
			$cantidadNueva = $cantidad;
			$totalNuevo = $cantidad * $fila['precioUnitario'];
			$stmt = $con->prepare("UPDATE carrito_detalle SET cantidad=?, total=? WHERE idCarrito=?;");
			$stmt->bindParam(1, $cantidadNueva);
			$stmt->bindParam(2, $totalNuevo);
			$stmt->bindParam(3, $idCarrito);
			
			if ($stmt->execute()) {
				$mensajeRespuesta = "Stock modificado correctamente";
			}else{
				$mensajeRespuesta = "Error al actualizar stock";
			}
		}else{
			$codigoRespuesta=1;
			$mensajeRespuesta = "El producto no existe en el carrito!";
		}

	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally{
		$json = array(
			'codigoRespuesta' => $codigoRespuesta,
			'mensajeRespuesta'=>$mensajeRespuesta,
			'totalNuevo' => $totalNuevo
		);
		echo json_encode($json);
	}
?>