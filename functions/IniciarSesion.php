<?php 
	include_once 'Conexion.php';
	extract($_POST);
	header("application/json");
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";
	$sesionAbierta = false;
	try {
		$stmt = $con->prepare("SELECT * FROM cliente where correoCliente = ? and claveCliente = ?");

		$stmt->bindParam(1, $email);
		$stmt->bindParam(2, $password);

		$stmt->execute();

		if ($fila = $stmt->fetch()) {
			$mensajeRespuesta = "¡Inicio sesion con exito!";
			$nombreCliente = $fila['nombreCliente'];
			$correoCliente = $fila['correoCliente'];
			$DNICliente = $fila['DNICliente'];
			if (!empty($cbSesion) && $cbSesion=="on") {
				$sesionAbierta = true;
			}
			session_start(); 
			$_SESSION['nombreCliente'] = $nombreCliente;
			$_SESSION['correoCliente'] = $correoCliente;
			$_SESSION['DNICliente'] = $DNICliente;
			$_SESSION['sesionAbierta'] = $sesionAbierta;
	    }else{
	    	$codigoRespuesta = 1;
	    	$mensajeRespuesta = "Correa o contraseña incorrecta";
	    }
	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally {
		$json = array(
			'codigoRespuesta' => $codigoRespuesta,
			'mensajeRespuesta'=>$mensajeRespuesta
		);
		echo json_encode($json);
	}
?>