<?php 
	include_once 'Conexion.php';
	extract($_POST);
	header("application/json");
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";
	$BuscarProductos = array();

	try {
		$stmt = $con->prepare("SELECT * FROM mueble m inner join mueble_tipo c on m.idTipoMueble = c.idTipoMueble where idMueble = ?");
		$stmt->bindParam(1, $idMueble);
		if ($stmt->execute()) {
			while ($fila = $stmt->fetch()) {
				$mensajeRespuesta = "Producto encontrado";
				$idMueble = $fila['idMueble'];
				$categoria = $fila['nombreTipo'];
				$materialMueble = $fila['materialMueble'];
				$precioMueble = $fila['precioMueble'];
				$nombreMueble = $fila['nombreMueble'];
				$stockMueble = $fila['stockMueble'];
				$fotoMueble = $fila['fotoMueble'];
				$producto = array(
					'idMueble' => $idMueble,
					'categoria'=>$categoria,
					'materialMueble'=>$materialMueble,
					'precioMueble'=>$precioMueble,
					'nombreMueble'=>$nombreMueble,
					'stockMueble'=>$stockMueble,
					'fotoMueble'=>$fotoMueble
				);
				array_push($BuscarProductos,$producto);
			}
			
	    }else{
	    	$codigoRespuesta = 1;
	    	$mensajeRespuesta = "No se encontraron productos";
	    }
	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally {
		$json = array(
			'codigoRespuesta' => $codigoRespuesta,
			'mensajeRespuesta'=>$mensajeRespuesta,
			'producto' => $BuscarProductos,
		);
		echo json_encode($json);
	}
?>
