<?php 
	include_once 'Conexion.php';
	header("application/json");
	session_start();

	extract($_POST);
	$dniCliente = $_SESSION['DNICliente'];
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";

	try {
		$stmt = $con->prepare("SELECT * FROM carrito_detalle WHERE dniCliente=? AND idMueble=?");
		$stmt->bindParam(1, $dniCliente);
		$stmt->bindParam(2, $idMueble);
		$stmt->execute();

		if ($fila = $stmt->fetch()) {
			$idCarrito = $fila["idCarrito"];
			$cantidadNueva = $fila["cantidad"]+$cantidad;
			$total = $fila["total"]+$precioUnitario;

			$stmt = $con->prepare("UPDATE carrito_detalle SET cantidad=?, total=? WHERE idCarrito = ?");
			$stmt->bindParam(1, $cantidadNueva);
			$stmt->bindParam(2, $total);
			$stmt->bindParam(3, $idCarrito);

			if ($stmt->execute()) {
				 $mensajeRespuesta = "Producto agregado al carrito"; 
			}else{
				$codigoRespuesta = 1;
				$mensajeRespuesta = "Error al agregar producto al carrito";
			}
		}else{
			$stmt = $con->prepare("INSERT INTO carrito_detalle(dniCliente, idMueble, cantidad, precioUnitario, total) VALUES(?,?,?,?,?);");
			$total = $cantidad*$precioUnitario;
			$stmt->bindParam(1, $dniCliente);
			$stmt->bindParam(2, $idMueble);
			$stmt->bindParam(3, $cantidad);
			$stmt->bindParam(4, $precioUnitario);
			$stmt->bindParam(5, $total);
			
			if($stmt->execute()) {
			   $mensajeRespuesta = "Producto agregado al carrito";
			}else{
				$codigoRespuesta = 1;
				$mensajeRespuesta = "Error al agregar producto al carrito";
			}
		}

	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally{
		$json = array('codigoRespuesta' => $codigoRespuesta,'mensajeRespuesta'=>$mensajeRespuesta);
		echo json_encode($json);
	}
?>