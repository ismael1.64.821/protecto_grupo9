<?php 
	include_once 'Conexion.php';
	header("application/json");
	session_start();

	extract($_GET);
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";

	$dniCliente = 74865889;//$_SESSION['DNICliente'];
	$compras = array();
	try {

		$stmt = $con->prepare("SELECT * FROM compra WHERE dniCliente = ? order by idCompra desc");
		$stmt->bindParam(1, $dniCliente);
		
		if ($stmt->execute()) {
			while($fila = $stmt->fetch()) {

				$idCompra = $fila['idCompra'];
				$dniCliente = $fila['dniCliente'];
				$fechaHora = $fila['fechaHora'];
				$precioFinal = $fila['precioFinal'];
				$productosCompra = array();
				$compraCabecera = array(
					'idCompra' => $idCompra, 
					'dniCliente' => $dniCliente, 
					'fechaHora' => $fechaHora, 
					'precioFinal' => $precioFinal,
					'productos'=> array()
				);
				$stmtDetalle = $con->prepare("SELECT nombreMueble,precioUnitario,cantidad, total FROM compra_detalle cd 
					INNER JOIN mueble m on cd.idMueble = m.idMueble WHERE idCompra = ?");
				$stmtDetalle->bindParam(1, $idCompra);
				
				if ($stmtDetalle->execute()) {
					
					while ($fila = $stmtDetalle->fetch()) {
						$nombreMueble = $fila["nombreMueble"];
						$precioUnitario = $fila["precioUnitario"];
						$cantidad = $fila["cantidad"];
						$total = $fila["total"];
						$producto = array(
								'nombreMueble' => $nombreMueble,
								'precioUnitario'=>$precioUnitario,
								'cantidad'=>$cantidad,
								'total'=>$total
							);
						//array_push($productosCompra,$producto);
						array_push($compraCabecera['productos'],$producto);
					}
					//array_push($compraCabecera['productos'],$productosCompra);
					array_push($compras,$compraCabecera);
					$mensajeRespuesta = "Compras cargadas correctamente;";
				}else{
					$codigoRespuesta=-1;
					$mensajeRespuesta = "Error al cargar productos de la compra";
				}
			}
		}else{
			$codigoRespuesta=-1;
			$mensajeRespuesta = "Error al obtener datos de compras";
		}

	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally{
		$json = array(
			'codigoRespuesta' => $codigoRespuesta,
			'mensajeRespuesta'=>$mensajeRespuesta,
			'compras' => $compras
		);
		echo json_encode($json);
	}

?>