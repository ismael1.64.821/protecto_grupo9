<?php 
	include_once 'Conexion.php';
	header("application/json");
	session_start();

	extract($_POST);
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";

	$dniCliente = $_SESSION['DNICliente'];
	$listaCarrito = array();

	try {
		$stmt = $con->prepare("SELECT idCarrito, dniCliente, nombreMueble, cantidad, precioUnitario, total FROM carrito_detalle c 
			INNER JOIN mueble m ON m.idMueble = c.idMueble 
			WHERE dniCliente = ?");
		$stmt->bindParam(1, $dniCliente);
		

		if ($stmt->execute()) {
			$mensajeRespuesta = "Carrito de compreas cargó con exito.";
			while ($fila = $stmt->fetch()) {
				$idCarrito = $fila["idCarrito"];
				$dniCliente = $fila["dniCliente"];
				$nombreMueble = $fila["nombreMueble"];
				$cantidad = $fila["cantidad"];
				$precioUnitario = $fila["precioUnitario"];
				$total = $fila["total"];
				$producto = array(
						'idCarrito' => $idCarrito,
						'dniCliente'=>$dniCliente,
						'nombreMueble'=>$nombreMueble,
						'cantidad'=>$cantidad,
						'precioUnitario'=>$precioUnitario,
						'total'=>$total
					);
				array_push($listaCarrito,$producto);
			}
		}else{
			$codigoRespuesta=1;
			$mensajeRespuesta = "Error al cargar carrito de compras.";
		}

	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally{
		$json = array(
			'codigoRespuesta' => $codigoRespuesta,
			'mensajeRespuesta'=>$mensajeRespuesta,
			'listaCarrito' => $listaCarrito
		);
		echo json_encode($json);
	}

?>