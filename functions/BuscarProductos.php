<?php 
	include_once 'Conexion.php';
	extract($_POST);
	header("application/json");
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";
	$BuscarProductos = array();
	$valorBusqueda = '%'.$valorBusqueda.'%';
	try {
		$stmt = $con->prepare("SELECT * FROM mueble WHERE nombreMueble like ?");
		$stmt->bindParam(1, $valorBusqueda);

		if ($stmt->execute()) {
			while ($fila = $stmt->fetch()) {
				$idMueble = $fila['idMueble'];
				$idTipoMueble = $fila['idTipoMueble'];
				$materialMueble = $fila['materialMueble'];
				$precioMueble = $fila['precioMueble'];
				$nombreMueble = $fila['nombreMueble'];
				$stockMueble = $fila['stockMueble'];
				$fotoMueble = $fila['fotoMueble'];
				$producto = array(
					'idMueble' => $idMueble,
					'idTipoMueble'=>$idTipoMueble,
					'materialMueble'=>$materialMueble,
					'precioMueble'=>$precioMueble,
					'nombreMueble'=>$nombreMueble,
					'stockMueble'=>$stockMueble,
					'fotoMueble'=>$fotoMueble
				);
				array_push($BuscarProductos,$producto);
			}
			
	    }else{
	    	$codigoRespuesta = 1;
	    	$mensajeRespuesta = "No se encontraron productos";
	    }
	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally {
		$json = array(
			'codigoRespuesta' => $codigoRespuesta,
			'mensajeRespuesta'=>$mensajeRespuesta,
			'BuscarProductos' => $BuscarProductos,
		);
		echo json_encode($json);
	}
?>