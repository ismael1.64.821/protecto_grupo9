<?php
	session_start();  
	include_once 'Conexion.php';
	extract($_POST);
	header("application/json");
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";
	$dniCliente = 74865889; //$_SESSION['DNICliente'] ;
	
	try {
		// Registrando cabezera de compra
		$stmt = $con->prepare("INSERT INTO compra (dniCliente, fechaHora, precioFinal) values(?,now(),?)");
		$stmt->bindParam(1, $dniCliente);
		$stmt->bindParam(2, $precioFinal);

		if($stmt->execute()) 
		{
			//Consultando idCompra de ultima compra registrada
			$stmt = $con->prepare("SELECT max(idCompra) idCompra FROM compra where dniCliente = ?");
			$stmt->bindParam(1, $dniCliente);
			if($stmt->execute()) {
				if ($fila = $stmt ->fetch()) {
					$idCompra = $fila['idCompra'];
					//Cargando Productos de carrito_detalle
					$stmtCar = $con->prepare("SELECT * FROM carrito_detalle WHERE dniCliente = ?");
					$stmtCar->bindParam(1, $dniCliente);
					if($stmtCar->execute()){
						while ($fila = $stmtCar->fetch()) {

							//Insertando los productos en la compra_detalle
							$stmtCD = $con->prepare("insert into compra_detalle(idCompra, idMueble, cantidad, precioUnitario, total) values (?,?,?,?,?)");
							$stmtCD->bindParam(1, $idCompra);
							$stmtCD->bindParam(2, $fila['idMueble']);
							$stmtCD->bindParam(3, $fila['cantidad']);
							$stmtCD->bindParam(4, $fila['precioUnitario']);
							$stmtCD->bindParam(5, $fila['total']);
							
							if($stmtCD->execute()) {
								$stmtCD = $con->prepare("DELETE FROM carrito_detalle WHERE dniCliente = ?");
								$stmtCD->bindParam(1, $dniCliente);
								if ($stmtCD->execute()) {
									$mensajeRespuesta = "Compra registrada exitosamente";
								}else{
									$codigoRespuesta = 6;
									$mensajeRespuesta = "Error al borrar productos del carrito";
								}
							}else{
								$codigoRespuesta = 5;
								$mensajeRespuesta = "Error al registrar productos en compra";
							}
						}
					} else {
						$codigoRespuesta = 4;
						$mensajeRespuesta = "Error al consultar productos del carrito";
					}
				} else {
					$codigoRespuesta = 3;
					$mensajeRespuesta = "idCompra no encontrado";
				}
			} else {
				$codigoRespuesta = 2;
				$mensajeRespuesta = "Error al consultar idCompra";
			}
		}
		else{
			$codigoRespuesta = 1;
			$mensajeRespuesta = "Error al registrar su compra";
		}

	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally{
		$json = array('codigoRespuesta' => $codigoRespuesta,'mensajeRespuesta'=>$mensajeRespuesta);
		echo json_encode($json);
	}
?>
