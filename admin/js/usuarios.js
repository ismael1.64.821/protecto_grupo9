var table = null;
function getPaginaUsuarios() {
    $("#idContent").html(`<div class="d-flex justify-content-center mt-5">
      <div class="spinner-border" role="status">
        <span class="visually-hidden">Loading...</span>
      </div>
    </div>`)

    $.get('src/pag/usuarios.html', function(data) {
        $("#idContent").html(data)
        $("#pagUsuario").addClass('active');
        
        table = $("#idTabla").DataTable({
			language: {
	            "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
	        },
			ajax: 'functions/CargarUsuarios.php',
			dom: 'B<"clear">lfrtip',
	        buttons: {
	        	name: 'primary',
	    		buttons: [ 'copy', 'csv', 'excel' ]
	        },
	        columns: [
	            { data: 'DniUsuario' },
	            { data: 'Nombres' },
	            { data: 'Apellidos' },
	            { data: 'Email' },
	            { data: 'Permisos' },
	            { data: 'estado' },
	            { data: 'Enlinea' },
	            { data: 'opciones' }
	        ]
		});


        //funciones
        $("#formNuevoUsuario").submit(function(event) {
        	if (validarFormulario('formNuevoUsuario')) {
        		$.post('functions/AgregarNuevoUsuario.php', $("#formNuevoUsuario").serialize(), function(data, textStatus, xhr) {
        			let response = JSON.parse(data);
        			$("#modalUsuario").modal('hide');
        			alert(response.mensajeRespuesta);
        		});
        	}
			event.preventDefault();
		});


    });
}

function abrirModalNuevoUsuario() {
	$("#btnAction").attr({
		onclick: 'agregarNuevoUsuario()'
	});
	$("#modalUsuario").modal('show');
}
function agregarNuevoUsuario() {
	$("#idUsuario").val("");
	$('#formNuevoUsuario').submit();
}
function abrirModalEditarUsuario(idUsuario) {
	$("#idUsuario").val(idUsuario);
	$("#btnAction").attr({
		onclick: 'editarUsuario('+idUsuario+')'
	});
	cargarDatosUsuario(idUsuario);
	$("#modalUsuario").modal('show');
}
function editarUsuario(idUsuario) {
	$.post('functions/EditarUsuario.php', $("#formNuevoUsuario").serialize(), function(data, textStatus, xhr) {
		let response = JSON.parse(data);
		if (response.codigoRespuesta==0) {
			$("#modalUsuario").modal('hide');
			$("#formNuevoUsuario")[0].reset();
			table.ajax.reload();
		}
		alert(response.mensajeRespuesta);
	});
}

function eliminarUsuario(idUsuario) {
	$.post('functions/EliminarUsuario.php',{idUsuario}, function(data, textStatus, xhr) {
		let response = JSON.parse(data);
		if (response.codigoRespuesta==0) {
			table.ajax.reload();
		}
		alert(response.mensajeRespuesta);
	});
}

function cargarDatosUsuario(idUsuario){
	$.post('functions/ObtenerDatosUsuario.php', {idUsuario}, function(data, textStatus, xhr) {
		let response = JSON.parse(data);
		if (response.codigoRespuesta==0) {
			$("#dniUsuarioNuevo").val(response.usuario.DniUsuario);
			$("#nombresUsuarioNuevo").val(response.usuario.Nombres);
			$("#apellidosUsuarioNuevo").val(response.usuario.Apellidos);
			$("#emailUsuarioNuevo").val(response.usuario.Email);
			$("#estadoUsuario").val(response.usuario.Estado);
			$("#permisosUsuarioNuevo").val(response.usuario.Permisos);
			$("#usernameUsuarioNuevo").val(response.usuario.Usuario);
			$("#passwordUsuarioNuevo").val(response.usuario.Password);
		}
	});
}



