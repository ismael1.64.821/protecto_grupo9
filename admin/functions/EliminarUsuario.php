<?php
	date_default_timezone_set('UTC');
	include 'Conexion.php';
	header("application/json");
	extract($_POST);
	session_start();
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";

	$usuarioSesion = $_SESSION['Usuario'];

	$stmt = $con->prepare("UPDATE usuario SET Estado='Eliminado',Fec_Modificacion=now(), Modificado_Por=? WHERE idUsuario=?;");


	$stmt->bindParam(1, $usuarioSesion);
	$stmt->bindParam(2, $idUsuario);

	if ($stmt->execute()) {
		$mensajeRespuesta = "Usuario eliminado correctamente";
	}else{
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error al eliminar usuario";
	}

	$json = array(
		'codigoRespuesta' => $codigoRespuesta,
		'mensajeRespuesta' => $mensajeRespuesta
	);

	echo json_encode($json)
?>