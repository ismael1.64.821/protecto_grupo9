<?php
	date_default_timezone_set('UTC');
	include 'Conexion.php';
	header("application/json");
	extract($_POST);
	session_start();
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";

	$usuario = null;
			$stmt = $con->prepare("SELECT * FROM usuario where idUsuario=?");
			$stmt->bindParam(1, $idUsuario);
			if ($stmt->execute()) {
				if ($fila = $stmt->fetch()) {
					$mensajeRespuesta = "Usuario encontrado";

					$idUsuario = $fila['idUsuario'];
					$DniUsuario = $fila['DniUsuario'];
					$Usuario = $fila['Usuario'];
					$Password = $fila['Password'];
					$Nombres = $fila['Nombres'];
					$Apellidos = $fila['Apellidos'];
					$Email = $fila['Email'];
					$Permisos = $fila['Permisos'];
					$Estado = $fila['Estado'];
					$usuario = array(
						'idUsuario' => $idUsuario,
						'DniUsuario' => $DniUsuario,
						'Usuario' => $Usuario,
						'Password' => $Password,
						'Nombres' => $Nombres,
						'Apellidos' => $Apellidos,
						'Email' => $Email,
						'Permisos' => $Permisos,
						'Estado' => $Estado
					);
				}else{
					$codigoRespuesta = 1;
			    	$mensajeRespuesta = "¡Usuario no existe!";
				}
			}else{
				$codigoRespuesta = -1;
		    	$mensajeRespuesta = "Error al consultar el usuario";
			}
			

	$json = array(
		'codigoRespuesta' => $codigoRespuesta,
		'mensajeRespuesta' => $mensajeRespuesta,
		'usuario' => $usuario
	);

	echo json_encode($json)
?>