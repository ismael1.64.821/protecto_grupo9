<?php 
	include_once 'Conexion.php';
	extract($_POST);
	header("application/json");
	session_start(); 
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";
	$espacioUsuario = "";
	try {
		session_destroy();
	} catch (Exception $e) {
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error: ".$e -> getMessage(); 
	} finally {
		$json = array(
			'codigoRespuesta' => $codigoRespuesta,
			'mensajeRespuesta'=>$mensajeRespuesta
		);
		//echo json_encode($json);
	}
?>
<h1>Cerrando sesion...</h1>
<script> 
	window.location.replace('../index.html'); 
</script>