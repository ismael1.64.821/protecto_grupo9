<?php
	date_default_timezone_set('UTC');
	include 'Conexion.php';
	header("application/json");
	extract($_POST);
	session_start();
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";

	$usuarioSesion = $_SESSION['Usuario'];

	$stmt = $con->prepare("INSERT INTO usuario(DniUsuario, Usuario, Password, Nombres, Apellidos, Email, Permisos, Estado, EnLinea, Num_Ingresos, Fec_Creacion, Creado_Por) VALUES(?,?,?,?,?,?,?,?,0,0,now(),?);");

	$stmt->bindParam(1, $dniUsuario);
	$stmt->bindParam(2, $usuario);
	$stmt->bindParam(3, $password);
	$stmt->bindParam(4, $nombres);
	$stmt->bindParam(5, $apellidos);
	$stmt->bindParam(6, $email);
	$stmt->bindParam(7, $permisos);
	$stmt->bindParam(8, $estado);
	$stmt->bindParam(9, $usuarioSesion);

	if ($stmt->execute()) {
		$mensajeRespuesta = "Usuario registrado correctamente";
	}else{
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error al agregar nuevo usuario";
	}

	$json = array(
		'codigoRespuesta' => $codigoRespuesta,
		'mensajeRespuesta' => $mensajeRespuesta
	);

	echo json_encode($json)
?>