<?php 
	date_default_timezone_set('UTC');
	include 'Conexion.php';
	header("application/json");
	extract($_POST);
	session_start();
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";

	if (array_key_exists('idUsuario', $_SESSION)) {
		$mensajeRespuesta = "Sesion iniciada";	
	}else{
		$codigoRespuesta = 1;
		$mensajeRespuesta = "Sesion NO iniciada";
	}

	$json = array(
		'codigoRespuesta' => $codigoRespuesta,
		'mensajeRespuesta' => $mensajeRespuesta,
	);

	echo json_encode($json)
?>