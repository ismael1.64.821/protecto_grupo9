<?php
	date_default_timezone_set('UTC');
	include 'Conexion.php';
	header("application/json");
	extract($_POST);
	session_start();
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";

	$cantidadIntentos = array_key_exists('cantidadIntentos',$_SESSION)?$_SESSION['cantidadIntentos']:0;
	//$cantidadIntentos = 0;//Descomentar para desbloquear los reitentos

	/* 
	//Calculando tiempo trascurrido desde el bloqueo
	$fechaInicioBloqueo =array_key_exists('horaUltimoIntento',$_SESSION)?$_SESSION['horaUltimoIntento']:new DateTime();
	$fechaUltimo = new DateTime();
	$fechadiff = $fechaUltimo->diff($fechaInicioBloqueo);
	$min = $fechadiff->i;*/

	
	//Validando que el usuario exista
	$stmt = $con->prepare("SELECT * FROM usuario where Usuario = ?");
	$stmt->bindParam(1, $usuario);
	$stmt->execute();
	if ($fila = $stmt->fetch()) {

		
		if ($fila['Estado']=='activo') {

			$stmt = $con->prepare("SELECT * FROM usuario where Usuario = ? and Password = ?");
			$stmt->bindParam(1, $usuario);
			$stmt->bindParam(2, $password);
			$stmt->execute();

			if ($fila = $stmt->fetch()) {
				$mensajeRespuesta = "¡Inicio sesion con exito!";
				$idUsuario = $fila['idUsuario'];
				$DniUsuario = $fila['DniUsuario'];
				$Usuario = $fila['Usuario'];
				$Nombres = $fila['Nombres'];
				$Apellidos = $fila['Apellidos'];
				$Email = $fila['Email'];
				$Permisos = $fila['Permisos'];

				$_SESSION['idUsuario'] = $idUsuario;
				$_SESSION['DniUsuario'] = $DniUsuario;
				$_SESSION['Usuario'] = $Usuario;
				$_SESSION['Nombres'] = $Nombres;
				$_SESSION['Apellidos'] = $Apellidos;
				$_SESSION['Email'] = $Email;
				$_SESSION['Permisos'] = $Permisos;

			}else{
		    	$cantidadIntentos += 1;
		    	$codigoRespuesta = 2;
		    	$mensajeRespuesta = "Usuario o contraseña incorrecta";
		    	$fechaUltimo = new DateTime();
		    	$_SESSION['horaUltimoIntento'] = $fechaUltimo;

		    	if ($cantidadIntentos>4){//No debe bloquear si no existe
					$stmt = $con->prepare("update usuario set Estado='bloqueado' where Usuario = ?");
					$stmt->bindParam(1, $usuario);
					$stmt->execute();
					$codigoRespuesta = 1;
					$mensajeRespuesta = "Usuario bloqueado - Contacte con un admin para desbloquearlo";
					session_destroy();
				}
		    }
		}else if($fila['Estado']=='eliminado'){

			$codigoRespuesta = 1;
			$mensajeRespuesta = "El usuario ha sido eliminado";
		}else if($fila['Estado']=='desactivo'){

			$codigoRespuesta = 1;
			$mensajeRespuesta = "Usuario se encuentra desactivado";
		}else{
			$codigoRespuesta = 1;
			$mensajeRespuesta = "Usuario bloqueado - Contacte con un admin para desbloquearlo";
		}

		
	}else{
		$codigoRespuesta = 3;
    	$mensajeRespuesta = "¡Usuario no existe!";
    	//$cantidadIntentos += 1;
	}

	$_SESSION['cantidadIntentos'] = $cantidadIntentos;

	$json = array(
		'codigoRespuesta' => $codigoRespuesta,
		'mensajeRespuesta' => $mensajeRespuesta,
		'cantidadIntentos' => (4-$cantidadIntentos)
	);

	echo json_encode($json)
?>