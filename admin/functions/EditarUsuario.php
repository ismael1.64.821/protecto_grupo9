<?php
	date_default_timezone_set('UTC');
	include 'Conexion.php';
	header("application/json");
	extract($_POST);
	session_start();
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";

	$usuarioSesion = $_SESSION['Usuario'];

	$stmt = $con->prepare("UPDATE usuario SET DniUsuario=?, Usuario=?, Password=?, Nombres=?, Apellidos=?, Email=?, Permisos=?, Estado=?,Fec_Modificacion=now(), Modificado_Por=? WHERE idUsuario=?;");

	$stmt->bindParam(1, $dniUsuario);
	$stmt->bindParam(2, $usuario);
	$stmt->bindParam(3, $password);
	$stmt->bindParam(4, $nombres);
	$stmt->bindParam(5, $apellidos);
	$stmt->bindParam(6, $email);
	$stmt->bindParam(7, $permisos);
	$stmt->bindParam(8, $estado);
	$stmt->bindParam(9, $usuarioSesion);
	$stmt->bindParam(10, $idUsuario);

	if ($stmt->execute()) {
		$mensajeRespuesta = "Usuario actualizado correctamente";
	}else{
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error actualizar usuario";
	}

	$json = array(
		'codigoRespuesta' => $codigoRespuesta,
		'mensajeRespuesta' => $mensajeRespuesta
	);

	echo json_encode($json)
?>