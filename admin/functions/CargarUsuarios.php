<?php 
	date_default_timezone_set('UTC');
	include 'Conexion.php';
	header("application/json");
	extract($_POST);
	session_start();
	$codigoRespuesta = 0;
	$mensajeRespuesta = "";

	$idUsuarioSesion = $_SESSION['idUsuario'];
	$stmt = $con->prepare("SELECT * FROM usuario where idUsuario<>?");
	$stmt->bindParam(1, $idUsuarioSesion);

	$usuarios = array();
	$data = array();
	if ($stmt->execute()) {
		while ($fila = $stmt->fetch()) {
			$idUsuario = $fila['idUsuario'];
			$DniUsuario = $fila['DniUsuario'];
			$Nombres = $fila['Nombres'];
			$Apellidos = $fila['Apellidos'];
			$Email = $fila['Email'];
			$Permisos = $fila['Permisos'];
			$estado = $fila['Estado'];
			$Enlinea = $fila['EnLinea'];
			$usuario = array(
				'idUsuario'=>$idUsuario,
				'DniUsuario'=>$DniUsuario,
				'Nombres'=>$Nombres,
				'Apellidos'=>$Apellidos,
				'Email'=>$Email,
				'Permisos'=>$Permisos,
				'estado'=>$estado,
				'Enlinea'=>$Enlinea,
				'opciones'=>'
				<button class="btn btn-sm btn-info" onclick="abrirModalEditarUsuario('.$idUsuario.')"><i class="bi bi-pencil-square"></i></button>
				<button class="btn btn-sm btn-danger" onclick="eliminarUsuario('.$idUsuario.')"><i style="font-size: .75rem" class="bi bi-person-x-fill"></i></button>'
			);
			array_push($data, $usuario);
		}
		$usuarios = array('data'=>$data);
		$mensajeRespuesta = "Usuarios cargados";
	}else{
		$codigoRespuesta = -1;
		$mensajeRespuesta = "Error al consultar usuarios";
	}

	$json = array(
		'data' => $data
	);

	echo json_encode($json)
?>