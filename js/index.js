$('input[type=radio][name=rbGenero]').change(function() {
	if(this.id == 'rbOtros') {
        $("#txtGenero").show('fast').val("");
    } else if(this.id == 'rbMasculino'){
    	$("#txtGenero").hide('fast').val("Masculino");
    }else if (this.id == 'rbFemenino') {
    	$("#txtGenero").hide('fast').val("Femenino");
    }
});

$("#txtclaveConfirmar").focusout(function(event) {
	let pass1 = $("#txtclave").val();
	let pass2 = $("#txtclaveConfirmar").val();
	if (pass1!=""||pass2!="") {
		if (pass2===pass1) {
			$("#txtclave").addClass('is-valid');
			$("#txtclaveConfirmar").removeClass('is-invalid').addClass('is-valid');
		}else{
			$("#txtclaveConfirmar").addClass('is-invalid');
		}
	}
	
});

function validarFormularioRegistro(idFormulario) {
	console.log('Iniciando validacion de formulario');
	let isvalid = true;
	let rows = $("#"+idFormulario+" :input").length;
	for (var i = 0; i < rows; i++) {
		if($("#"+idFormulario+" :input")[i].checkValidity()){
			$("#"+$("#"+idFormulario+" :input")[i].id).addClass("is-valid").removeClass('is-invalid');
		}else {
			$("#"+$("#"+idFormulario+" :input")[i].id).addClass("is-invalid").removeClass('is-valid');
			isvalid = false;
		}
	}
	return isvalid;
}


$("#btnEnviarFormNuevoCliente").click(function(event) {
	let pass1 = $("#txtclave").val();
	let pass2 = $("#txtclaveConfirmar").val();
	if(validarFormularioRegistro('formClienteNuevo')){
		if(pass2===pass1){
			$.post('functions/RegistrarCliente.php', $("#formClienteNuevo").serialize(), function(data, textStatus, xhr) {
				let response = JSON.parse(data);
				if (response.codigoRespuesta == 0) {
					$("#modalRegistro").modal("hide");

					$.post('functions/IniciarSesion.php', {email: $("#txtCorreoCliente").val(), password:$("#txtclave").val(), cbSesion:'on'} , function(data, textStatus, xhr) {
						let response = JSON.parse(data);
						if (response.codigoRespuesta==0) {
							console.log(response.mensajeRespuesta);
							console.log('Iniciando sesion');
							location.reload();
						}
					});

				}else{
					alert(response.mensajeRespuesta);
				}
			});
			console.log('Es valido');
		}else {
			$("#txtclaveConfirmar").addClass('is-invalid');
		}
	}else{
		if(pass2!=pass1){
			$("#txtclaveConfirmar").addClass('is-invalid');
		}
	}
});

function iniciarSesion(){
	console.log('Iniciando validacion de sesion');
	if (validarFormulario("formLogin")) {
		
		$.post('functions/IniciarSesion.php', $("#formLogin").serialize() , function(data, textStatus, xhr) {
			let response = JSON.parse(data);
			if (response.codigoRespuesta==0) {
				console.log('Iniciando sesion');
				location.reload();
			}
			console.log(response.mensajeRespuesta);
		});
	}
}

$("#btnIniciarSesion").click(function(event){
	iniciarSesion()
});

$("#txtCorreoLogin,#txtPassLogin").keypress(function(e) {
	if(e.which == 13) {
	  iniciarSesion();
	}
});