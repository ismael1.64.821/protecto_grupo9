function validarFormulario(idFormulario) {
	$("#"+idFormulario).addClass('was-validated');
	return $("#"+idFormulario)[0].checkValidity();
}

function mostrarMensaje(mensaje, tipo) {
		let alerta = ``;
		if (tipo=="danger") {
			alerta = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
  						<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg> ${mensaje}
						 <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>`;

		}else if(tipo=="success"){
			alerta = `<div class="alert alert-success alert-dismissible fade show" role="alert">
  						<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg> ${mensaje}
						 <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>`;

		}else if(tipo=="warning"){
			alerta = `<div class="alert alert-warning alert-dismissible fade show" role="alert">
  						<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg> ${mensaje}
						 <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>`;
		}else{
			alerta = `<div class="alert alert-primary alert-dismissible fade show" role="alert">
  						<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg> ${mensaje}
						 <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>`;
		}
		$("#alertas").append(alerta);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}